from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from home import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('', include('home.urls')),
    path('post/', include('post.urls')),
    path('blog/', views.blog, name='blog'),
    path('post/<int:id>/<slug:slug>', views.post_detail, name='post_detail'),
    path('category/<int:id>/<slug:slug>', views.category_detail, name='category_detail'),

    path('contact/', views.contact, name='contact')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
