# Blog YouTube

## 1 - Preparando ambiente

1 - Instalando ambiente virtual 

Instalar virtualenv

    pip3 install virtualenv

Criar ambiente virtual

    virtualenv [nome_do_projeto]

ou

    python3 -m virtualenv [nome_do_projeto]


2 - Ativando ambiente

    source [nome_do_projeto]/bin/activate


## 2 - Instalando Django

Ver os pacotes que temos instalados

    pip3 list

Instalar o django

    pip3 install django

## 3 - Iniciando projeto

Instalar projeto raiz

    django-admin startproject core
ou

    django-admin startproject core .

Criando arquivo .gitignore

## 4 - Adicionado postgres como banco de dados

Criando docker-compose.yml

Subindo imagem
    docker-compose up -d

Instalando dependências do postgres no Django

    pip3 install psycopg2-binary 

Mudando DATABASES

## 5 - Comandos inciais 

python3 manage.py migrate

Criar novo usuário

python3 manage.py createsuperuser


## 6 - Estruturando projeto

Explicação da estrutura a ser usada
Adicionado apps no settings

Criando url index em home
colocar urls de home no url de settings
Criando primeira view index

## 7 Criando aplicações
Criando aplicação home e post

django-admin startapp home
django-admin startapp post


## 7 - Adicionado HTML

Link do template usado

Separar em navbar, slide, banner ,content, aside,  preloader, footer, index e homebase 

Criar pasta static
Adicionando arquivos static (criar pastas vendor, assets)

Criando página homebase (dividir aside de post)

criar index.html

configurar view

Não esquecer de dar include no html


## 8 - Criando models

criar model de Categoria,

comando: makemigrations

criando admin

criando model de postagem

image_url

admin post


## 9 - Imagens não aparecendo

Instalar Pillow 

    pip3 install pillow

Adicionar no urls

    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


## 10 - Ckeditor

Instalar Ckeditor

    pip3 install django-ckeditor

Instalar dependência no settings

    'ckeditor',
    'ckeditor_uploader',

Adicionar configuração do ckeditor no settings


Adicionar url do ckeditor no urls.py
    path('ckeditor/', include('ckeditor_uploader.urls')),


## 11 - View index.html

Criar primeira view 

Arrumar content.html 

## 12 - Criando nova url blog

    Paginação

```
     <div class="col-lg-12">
                            <ul class="page-numbers">

                                {% if page_obj.has_previous %}
                                <li><a href="?page={{ page_obj.previous_page_number }}"><i
                                            class="fa fa-angle-double-left"></i></a></li>
                                {% endif %}

                                <li class="active">{{ page_obj.number }}</li>

                                {% if page_obj.has_next %}
                                <li><a href="?page={{ page_obj.next_page_number }}"><i
                                            class="fa fa-angle-double-right"></i></a></li>
                                {% endif %}
                            </ul>
                        </div>

```

## 13 - Postagem detalhada

Criando url da postagem detalhada.

Criando view

Para arrumar imagem fora do espaçamento
```
 <div class="blog-thumb">
    {{post.text|safe}}
</div>
```


## 14 - Fazendo comentários nas postagens

Criar  model de formulário para fazer comentário nas postagens

criar forms.py

Criar url 

Criar view

Adicionar model de Comment em post_detail

token no form do html

Listando comentários

## 15 - Criando página de contato

Criar model de contato (nome, email, subject, message)

Explicar diferença entre null e blank (null está relacionado aos valores que podem ser nulos ou não na tabela, e blank se os valores do formulário podem ser brancos ou não)

criar formulário

criar url de contato

Criar view

Criar admin

## 16 - Customizando aside

## 17 - Criando página para listar categorias

## 18 - Slide

## Deploy


Instalar o guicortn
