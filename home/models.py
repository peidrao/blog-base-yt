from django.db import models

# Create your models here.

class Contact(models.Model):
    name = models.CharField(max_length=150, blank=False)
    email = models.CharField(max_length=150, blank=False)
    subject = models.CharField(max_length=150, blank=False)
    message = models.TextField(blank=False)

    def __str__(self):
        return self.name
    