from django.shortcuts import render, HttpResponseRedirect, HttpResponse
from django.core.paginator import Paginator

# Create your views here.

from post.models import Category, Post, Comment
from .models import Contact
from .forms import ContactForm

def index(request):
    category = Category.objects.all()
    post_latest = Post.objects.all().order_by('-id')[:3]
    post_random = Post.objects.all().order_by('?')[:5]
    
    context = {
        'category': category,
        'post_latest': post_latest,
        'post_random': post_random
        }
    return render(request, 'index.html', context)

def blog(request):
    post_latest = Post.objects.all().order_by('-id')[:3]
    post = Post.objects.all()
    category = Category.objects.all()

    paginator = Paginator(post, 4)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {
        'page_obj': page_obj,
        'post_latest': post_latest,
        'category': category
    }
    
    return render(request, 'pages/blog.html', context)


def post_detail(request, id, slug):
    query = request.GET.get('q')
    category = Category.objects.all()
    post = Post.objects.get(pk=id)
    comments = Comment.objects.filter(post_id=id, status='Lido')
    total = 0
    for i in comments:
        total = total + 1

    context = {
        'category' : category,
        'post' : post,
        'comments' : comments,
        'total': total,
    }

    return render(request, 'pages/post_detail.html', context)


def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            data = Contact()
            data.name = form.cleaned_data['name']
            data.email = form.cleaned_data['email']
            data.subject = form.cleaned_data['subject']
            data.message = form.cleaned_data['message']

            data.save()
            return HttpResponseRedirect('/contact')
    form = ContactForm
    return render(request, 'pages/contact.html', {'form': form})


def category_detail(request, id, slug):
    category = Category.objects.all()
    post = Post.objects.filter(category_id = id)
    post_latest = Post.objects.all().order_by('-id')[:3]
    paginator = Paginator(post, 4)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {'post':post, 'page_obj': page_obj, 'category': category, 'post_latest': post_latest}

    return render(request, 'pages/category_detail.html', context)