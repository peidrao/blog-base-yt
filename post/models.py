from django.contrib.auth.models import User
from django.db import models
from django.utils.safestring import mark_safe
from django.urls import reverse
from ckeditor_uploader.fields import RichTextUploadingField

# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length=150)
    slug = models.SlugField(unique=True, null=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("category_detail", kwargs={"slug": self.slug})
    

class Post(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=False)
    
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    title = models.CharField(max_length=150, null=False)
    subtitle = models.CharField(max_length=150, null=False)
    description = RichTextUploadingField()
    image = models.ImageField(upload_to='images/', null=False)
    text = RichTextUploadingField()
    slug = models.SlugField(unique=True, null=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


    def get_absolute_url(self):
        return reverse("post_detail", kwargs={"slug": self.slug})

    def image_tag(self):
        if self.image.url is not None:
            return mark_safe('<img src="{}" height="50"/>'.format(self.image.url))
        else:
            return ""



class Comment(models.Model):
    STATUS = (
        ('Lido', 'Lido'),
        ('Não Lido', 'Não Lido'),
    )
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    name = models.CharField(max_length=250, blank=True)
    subject = models.CharField(max_length=250, blank=True)
    email = models.CharField(max_length=250, blank=True)
    comment = models.TextField()
    status = models.CharField(choices=STATUS, max_length=10, default='Não Lido')
    
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


    def __str__(self):
        return self.subject




    