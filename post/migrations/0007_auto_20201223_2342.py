# Generated by Django 3.1.4 on 2020-12-23 23:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('post', '0006_auto_20201223_2336'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='status',
            field=models.CharField(choices=[('Lido', 'Lido'), ('Não Lido', 'Não Lido')], default='Não Lido', max_length=10),
        ),
    ]
